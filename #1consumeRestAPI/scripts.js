const app = document.getElementById('root');
const container = document.createElement('div');
container.setAttribute('class', 'container');
app.appendChild(container);
var request = new XMLHttpRequest();
request.open('GET', 'https://rickandmortyapi.com/api/character/', true);
request.onload = function () {
  // Begin accessing JSON data here
  var data = JSON.parse(this.response);
  if (request.status >= 200 && request.status < 400) {
  	for (var i = 0; i < data.results.length; i++) {
    const card = document.createElement('div');
    card.setAttribute('class', 'card');
		const h1 = document.createElement('h1');
		h1.textContent = data.results[i].name;
		const logo = document.createElement('img');
		logo.src = data.results[i].image;
		const p = document.createElement('p');
		p.textContent = `Gender: ${data.results[i].gender}`;
		container.appendChild(card);
		card.appendChild(h1);
		card.appendChild(logo);
		card.appendChild(p);
    console.log(data.results[i]);
  	} 
  } else {
   	console.log('error');
  }
}
request.send();